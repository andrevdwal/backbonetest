(function($){

  var AppView = Backbone.View.extend({

    el: $('body'),

    initialize: function(){
      _.bindAll(this, 'render');
      this.render();
    },

    render: function(){
      $(this.el).append("<h1>This is backbone.js</h1>");
      $(this.el).append("<small>And it is from an NPM project</small>");
    }
  });

  var appView = new AppView();
})(jQuery);
