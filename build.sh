#!/bin/bash

echo starting deployment

# vars
BASE=`dirname ${0}`
TAB="  "
NODEDIR="$BASE/node_modules"
DISTDIR="${BASE}/dist"

echo - cleaning
rm -rf $DISTDIR
mkdir $DISTDIR

declare -a arrNodeToDistFile=(
  "jquery/dist/jquery.min.js"
  "backbone/backbone-min.js"
  "underscore/underscore-min.js"
)

for fil in "${arrNodeToDistFile[@]}"
do
  echo "${TAB} - ${NODEDIR}/${fil}"
  mkdir -p `dirname ${DISTDIR}/${fil}`
  cp $NODEDIR/${fil} `dirname ${DISTDIR}/${fil}/`
done

echo - removing .DS_Store files
find $DISTDIR -name '.DS_Store' -type f -delete

echo done
